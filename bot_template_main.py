__version__ = '<your bot version>'
# Imports
import discord # Basic discord.py imports
from discord.ext import commands # ^

import random # This is needed for the changing of activities
import asyncio # And this too

import json # This is needed to decode the data held in the JSON file

import logging # Required for the logging 
import traceback # Used in the extension loading to get the full error traceback

from textwrap import dedent # This is used to make the ready message code be beatiful while also looking great with the rest of the code
import sys # Needed to get the system info in the ready message

# Setting up logging
logging.basicConfig(level='INFO') # Basic configuration to 'INFO' level. See 'https://docs.python.org/3/library/logging.html#logging-levels' all the available levels
logger = logging.getLogger('some_cool_name_related_to_your_bot') # Creating a logger so we can later dump errors, warnings, etc 


# Getting your bot basic data from a external file so that you can share your code easily without having to always black it out
with open('BOTBASICDATA.json') as json_fp: # Don't forget to check my BOTBASICDATA.json template file
    basic_data = json.load(json_fp) # Loading data from the json file
    TOKEN = basic_data["token"] # Getting the token
    prefix = basic_data["prefix"] # Getting the prefix
    extensions = basic_data["extensions"] # Getting the extensions to load


bot = commands.Bot(description="<your bot description>", command_prefix=prefix)


# Removing the default help command so that you can make a better and fancier one later. Not mandatory of course.
# bot.remove_command('help')


# This piece of code loads all the extensions delcared on the 'extensions' varaiable
for ext in extensions:
   try:
       bot.load_extension(ext) # Trying to load the extension
   except Exception:
       logger.error(f'\nFailed to load {ext} with error:\n{traceback.format_exc()}') # Send an error to the logger if something goes wrong during the loading


# I also have another extension loading snippet. This one is a little bit more fancy and uses the "extensions.txt" file
# With this you can have commented lines, internal and external cogs.
# Comment lines start with a # , just like in Python.
# Internal cogs start with a * . Specify the path (Python-styled path (dot notation)) to your extensions/cogs folder in "cogs_dir".
# If you have your cogs/extensions in the same folder as your main file, leave that variable as it is
# External cogs don't have any prefix. Just specify it's path (i.e, "libneko.extras.help")
cogs_dir = ""
cogs = []
with open("extensions.txt") as cog_file: # Opening hte file similar to how it's done for the basic data JSON file
    for line in cog_file.read().split('\n'): # Iterating through the read and splitted by new lines list
        line = line.strip() # Removing trailing spaces
        if not line or line.startswith('#'): # Detecting if the line is empty or commented out
            continue # Ignore this line if so
        if line.startswith('*'): # Checking for internal extensions
            line = f"{cogs_dir}{line[1:]}" # Adding the prefix for loading
        cogs.append(line) # Adding this extension to the list

for cog in cogs: # Now it's time to actually load the cogs
    try:
        bot.load_extension(cog) # Loading the cog
        logger.info(f"Loaded: {cog}") # Sending out a message saying that this cog was successfuly loaded
    except BaseException as ex:
        logger.error(f"Could not load {cog}\n{traceback.format_exception(type(ex), ex, ex.__traceback__)}") # If not just send out an error specifying the error

### NOTE: Be sure to just choose one of these methods! ###


async def change_activities():
    """Quite self-explanatory. It changes the bot activities"""
    timeout = 60  # The time between each change of status in seconds
    playopt = ('Minesweeper', 'Pong', 'Tic-tac toe', 'with the Discord API') # All the options for the "Playing <something>" status
    watchopt = (f'for {prefix}help') # All the options for the "Watching <something>" status
    streamopt = (' bits of information') # All the options for the "Streaming <something>" status
    listenopt = ('to beeps and bops') # All the options for the "Listening <something>" status
    while True: # Infinite loop
        game = discord.Game(name=random.choice(playopt)) # Pick a choice from 'playopt'. 
                                                         # Watch out for how you import 'random.choice', as that might affect how this line needs to be written. 
                                                         # For more help refer to the Python Docs.
        watch = discord.Activity(type=discord.ActivityType.watching, name=random.choice(watchopt)) # Pick a choice from 'watchopt'
        stream = discord.Streaming(url="<your twitch link>", name=random.choice(streamopt)) # Pick a choice from 'streamopt'
        listen = discord.Activity(type=discord.ActivityType.listening, name=random.choice(listenopt)) # Pick a choice from 'listenopt'
        possb = random.choice([watch, stream, game, listen]) # Pick a choice from all the possibilities: "Playing", "Watching", "Streaming", "Listening" or "Playing"
        await bot.change_presence(activity=possb) # Now change the status to the seletected activity
        await asyncio.sleep(timeout) # And wait for 'timeout' seconds


@bot.listen()
async def on_ready():
    """This function will be executed when the bot is fully ready."""

    # Setting up a creator attribute. With this you can very easily get your user just by doing 'bot.creator'
    creator = (await bot.application_info()).owner
    setattr(bot, 'creator', creator)

    setattr(bot, 'logger', logger) # Setting a logger attribute to 'bot' so that you can easily send stuff to it just by doing 'bot.logger...' 

    print(dedent(
        f"""
        ===========================
        I'm ready!
        Welcome to {bot.user.name} !
        ID: {bot.user.id}
        Creator: {creator}
        Current prefix: {prefix}
        Python Version: {sys.version[:6]}
        Discord Version: {discord.__version__}
        {bot.user.name} Version: {__version__}
        ===========================
        """)
    )

    bot.loop.create_task(change_activities())  # To fire up the activity changer worker in the background

    # This part will send a message to the given channel (OPTION 1) or a DM to you (OPTION 2)
    channel = bot.get_channel(1234567890) # Gets a discord.TextChannel from the id you insert. YOU MUST INSERT A INT ### OPTION 1
    await channel.send(embed=discord.Embed(
        description=f":white_check_mark: **{bot.user.name} successfuly booted-up!**", 
        color=discord.Colour.green())) # And sends a pretty embed there ### OPTION 1
    await creator.send(embed=discord.Embed(
        description=f":white_check_mark: **{bot.user.name} successfuly booted-up!**", 
        color=discord.Colour.green())) ### OPTION 2



####################################################
# Here's where you put all the stuff you wanna do. #
####################################################
@bot.command()
async def say(ctx, *, msg: str):
    await ctx.send(msg)


# Execution
bot.run(TOKEN) # Actually running the bot running

